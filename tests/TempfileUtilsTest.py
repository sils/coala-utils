import os
import unittest
import tempfile

from coala_utils.tempfile_utils import (create_tempfile)


class TempfileUtilsTest(unittest.TestCase):

    def test_create_tempfile(self):
        tpath = create_tempfile()
        self.assertTrue(os.path.isfile(tpath))
        self.assertTrue(os.path.basename(tpath).startswith("tmp"))
        os.remove(tpath)

        with tempfile.TemporaryDirectory() as tdir:
            tpath = create_tempfile(suffix=".old", prefix="ttest", dir=tdir)
            self.assertTrue(os.path.isfile(tpath))
            self.assertTrue(os.path.basename(tpath).startswith("ttest"))
            self.assertTrue(tpath.endswith(".old"))
            self.assertTrue(os.path.basename(tpath) in os.listdir(tdir))
            os.remove(tpath)
